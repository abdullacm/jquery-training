$(document).ready(function() {
    $('select').select2();

    $('.bxslider').bxSlider({
        captions: true, 
        slideWidth: 500, 
        mode: 'vertical', 
        onSliderLoad: function() {
            console.log('loaded');
        },
        onSlideAfter: function(a, b, c) {
            console.log(a, b, c)
        }
    });
});