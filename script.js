$(document).ready(function() {
    //console.log('test');
    
    $(document).on('click', '#test-btn', function() {
       
        $(this).addClass('btn-color');
        //var count = parseInt($(this).attr('data-clicked'));
        //$(this).attr('data-clicked', ++count);

        var count = parseInt($(this).data('clicked'));
        $(this).data('clicked', ++count);


        $('span').addClass('red-color');

        $('p.red-color').removeClass('red-color').addClass('green-color');

        

        if ($('div[user] input').length == 0) {
            var div = $('div[user]');
            for (var i = 0; i < 5; i++) {
                div.append('<input type="checkbox" />');
            }
        }        

        $('p.result').html($(this).data('clicked'));
    });

    $(document).on('click', '.add-row', function() {
        /*$('table tbody').append(
            '<tr><td><input type="text" /></td>' +
            '</tr>'
        );*/
        var row = $('table tbody tr:last').clone();
        $(row).find('.add-row').remove();
        $(row).find('td:last').html('<button class="remove">x</button>');
        $(row).find('input').val('');
        $(row).removeClass('odd even');
        
        if (($('table tbody tr').length + 1) % 2 == 0) {
            $(row).addClass('even');
        } else {
            $(row).addClass('odd');
        }

        $('table tbody').append(row);

        $('.c-program:last').
            val(Math.ceil(Math.random() * 10)).
            focus();

        calculate();
    });

    $(document).on('click', '.remove', function() {
        $(this).closest('tr').remove();
        calculate();
    });

    $(document).on('keyup', '.c-program', function() {
        calculate();
    });
});

function calculate()
{
    var total = 0;
    $('.c-program').each(function(i, el) {
        var currentValue = parseInt($(el).val());
        if (!isNaN(currentValue)) {
            total += currentValue;
        }
        //total = total + $(el).val();
    });
    //$('input.total').val(total);
    $('.td-total').html(total);
}

/*$(document).ready(function() {
    console.log(Math.random());
});

$(function() {
    console.log('shorter');
});*/

//$('.add-row').text()
//$('.add-row').text('Add Row')
//$('div[user]').append('<input type="radio" />')
//$('div[user]').prepend('<input type="radio" />')
//$('input[type="checkbox"]:last')
//$('input[type="checkbox"]:first').before('<input type="text" />')
//$('input[type="checkbox"]:first').after('<input type="text" />')
//$('input[type="checkbox"]')
//$('input[type="checkbox"]:eq(0)')
//$('input[type="checkbox"]:eq(2)').attr('checked', true)